This is a basic maven microservice archetype. To create a project using this archetype clone this repo, then: 

1) go the the project folder, then execute:

~~~
mvn archetype:create-from-project
~~~

2) go to project /target until you reach the root folder of the project created there. Then execute:

~~~
mvn installl
~~~

3) Lastly, create a new folder for your new project in your repos folder, then execute:

~~~
mvn archetype:generate -DarchetypeCatalog=local  -DarchetypeGroupId=com.jbdev.microarchetype -DarchetypeArtifactId=microservice-archetype-archetype -DarchetypeVersion=1.0-SNAPSHOT -DgroupId=com.jbdev.{project-name} -DartifactId={artifact-project-name} -Dversion={your-project-initial-version}
~~~

If you wish to update the archetype you can always edit the code cloned from this repo and re-execute the steps listed.